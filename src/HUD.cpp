#include "HUD.h"
#include "constants.h"


HUD::HUD()
{
    bool fontError = font_.loadFromFile(PATH + "fixedsys.ttf");
	if (!fontError)
		std::cout << "Font load error" << '\n';

	THealth_.setFont(font_);
	TDamage_.setFont(font_);
	TXY_.setFont(font_);
    TVelocity_.setFont(font_);

	THealth_.setCharacterSize(20);
	TDamage_.setCharacterSize(20);
	TXY_.setCharacterSize(20);
    TVelocity_.setCharacterSize(20);

    TVelocity_.setFillColor(Color::Black);
    THealth_.setFillColor(Color::Black);
    TXY_.setFillColor(Color::Black);
    TDamage_.setFillColor(Color::Black);
}

HUD::~HUD()
{

}

void HUD::display(RenderWindow& window, HeroInfo& heroInfo, View& view)
{
	SHealth_ = "Health: ";
	SDamage_ = "Damage: ";
	SXY_ = "Coords: ";
    SVelocity_ = "Velocity: ";

    THealth_.setPosition(view.getCenter().x - 930.0f, view.getCenter().y - 520.0f);
    TDamage_.setPosition(view.getCenter().x - 930.0f, view.getCenter().y - 500.0f);
    TXY_.setPosition(view.getCenter().x - 930.0f, view.getCenter().y - 480.0f);
    TVelocity_.setPosition(view.getCenter().x - 930.0f, view.getCenter().y - 460.0f);
	
	SHealth_ += heroInfo.currHealth + " / " + heroInfo.maxHealth;
	SDamage_ += heroInfo.damage;
	SXY_ += heroInfo.x + " , " + heroInfo.y;
    SVelocity_ += heroInfo.vel_x + " , " + heroInfo.vel_y;

	THealth_.setString(SHealth_);
	TDamage_.setString(SDamage_);
	TXY_.setString(SXY_);
    TVelocity_.setString(SVelocity_);

	window.draw(THealth_);
	window.draw(TDamage_);
	window.draw(TXY_);
    window.draw(TVelocity_);
}

