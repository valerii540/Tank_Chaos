#pragma once

#include "Map.h"
#include "Hero.h"
#include "HUD.h"
#include "Enemy.h"
#include "constants.h"
#include <SFML/Graphics.hpp>
#include <list>

class Level
{
public:
    Level();
    ~Level();
	
    bool	start();
	

private:
    bool    showEndingScreen(View&, RenderWindow&, bool);

    Hero*               hero_;
    std::list<Enemy*>   enemiesList_;

    Map*                map_;
    RenderWindow*       window_;
    HUD*                hud_;
    b2World*            world_;
    View*               view_;
    bool                restartFlag_;
    int                 difficulty_;
};
