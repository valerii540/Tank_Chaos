#include "MainMenu.h"
#include "constants.h"
#include "GameLoop.h"

MainMenu::MainMenu()
{
	backTexture_ = new Texture();
    bool textureError = backTexture_->loadFromFile(PATH + "MenuBackground.jpg");
	if (!textureError)
		std::cout << "Texture load error" << '\n';
	else
		background_.setTexture(*backTexture_);
	
    bool fontError = font_.loadFromFile(PATH + "fixedsys.ttf");
	if (!fontError)	
		std::cout << "Font load error" << '\n';

	setLabelStyle(l1Start_,      "Start game",   510.f, 200.f);
	setLabelStyle(l2Settings_,   "Settings",     530.f, 300.f);
	setLabelStyle(l3Exit_,		 "Exit",         560.f, 400.f);

    setLabelStyle(l1Difficulty_, "Difficulty: EASY", 470.f, 200.f);
	setLabelStyle(l2Back_,       "Back",         560.f, 400.f);
}

MainMenu::~MainMenu()
{
	delete backTexture_;
}

void MainMenu::setLabelStyle(Text& label, String string, float x, float y)
{
	label.setFont(font_);
	label.setString(string);
	label.setPosition(x, y);
}

bool MainMenu::display()
{
	RenderWindow window(VideoMode(1200, 800), "Tanks Chaos Launcher", Style::Default);
	
	bool main_tab = true;
	bool is_click = true;
	bool start_game = true;
	
	bool menu_on = true;
	while (menu_on) {
		window.clear();

		Event event;
		while (window.pollEvent(event))
			if (event.type == Event::Closed) {
                menu_on = false;
                start_game = false;
				window.close();
			}

		if (main_tab && menu_on) {
			int menu_num = 0;
			l1Start_.setFillColor(Color::White);
			l2Settings_.setFillColor(Color::White);
			l3Exit_.setFillColor(Color::White);

			if (IntRect(l1Start_.getGlobalBounds()).contains(Mouse::getPosition(window))) {
				l1Start_.setFillColor(Color::Red); 
				menu_num = 1; 
			}
			if (IntRect(l2Settings_.getGlobalBounds()).contains(Mouse::getPosition(window))) { 
				l2Settings_.setFillColor(Color::Red); 
				menu_num = 2; 
			}
			if (IntRect(l3Exit_.getGlobalBounds()).contains(Mouse::getPosition(window))) {
				l3Exit_.setFillColor(Color::Red); 
				menu_num = 3; 
			}

			if (Mouse::isButtonPressed(Mouse::Left) && is_click)	// Check for pressing
				switch (menu_num) {
					case 1 : 
						menu_on = false;
						window.close();
						break;
				
					case 2 : 
						main_tab = false; 
                        sleep(milliseconds(100));
						break;
				
					case 3 :
						start_game = false; 
						menu_on = false; 
						window.close();
				}
			if (Mouse::isButtonPressed(Mouse::Left))	// Check for click
				is_click = false;
			else
				is_click = true;

			window.draw(background_);
			window.draw(l1Start_);
			window.draw(l2Settings_);
			window.draw(l3Exit_);

		}
		else if (menu_on) {
			int menu_num = 0;
			l1Difficulty_.setFillColor(Color::White);
			l2Back_.setFillColor(Color::White);

			if (IntRect(l1Difficulty_.getGlobalBounds()).contains(Mouse::getPosition(window))) { 
				l1Difficulty_.setFillColor(Color::Red); 
				menu_num = 1; 
			}
			if (IntRect(l2Back_.getGlobalBounds()).contains(Mouse::getPosition(window))) { 
				l2Back_.setFillColor(Color::Red); 
				menu_num = 2; 
			}

			if (Mouse::isButtonPressed(Mouse::Left) && is_click)
				switch (menu_num) {
					case 1 : 
                        if (GameLoop::difficulty_ == 1) {
                            GameLoop::difficulty_ = 2;
                            l1Difficulty_.setString("Difficulty: HARD");
                        }
                        else {
                            GameLoop::difficulty_ = 1;
                            l1Difficulty_.setString("Difficulty: EASY");
                        }
                        sleep(milliseconds(100));
						break;
					case 2 : 
						main_tab = true; 
                        sleep(milliseconds(100));
				}
			if (Mouse::isButtonPressed(Mouse::Left))
				is_click = false;
			else
				is_click = true;

			window.draw(background_);
			window.draw(l1Difficulty_);
			window.draw(l2Back_);
		}
		window.display();
	}

	return start_game;
}
