#pragma once

#include <SFML/Graphics.hpp>
#include <Box2D.h>
#include <iostream>
#include <string>
using namespace sf;

class Entity
{
public:
                    Entity() {}
    virtual			~Entity() {}
	
    virtual void	attack(b2World&) = 0;
	virtual void	takeDamage(int) = 0;
	virtual void	display(RenderWindow&) = 0;
    virtual void    moving(b2World&) = 0;
    std::string     getNameId() { return nameId_; }

protected:
    std::string nameId_;
    int         maxHealth_;
    int         currentHealth_;
    int         damage_;
    float       speed_;
    bool        iDead_;

    Clock       clockOfFire_;
    Time        rateOfFire_;

    float       x_, y_;
    char        direction_;

	Texture*	entityTexture_;
	Sprite		entitySprite_;
    b2Body*     entityPhysicsBody_;
};
