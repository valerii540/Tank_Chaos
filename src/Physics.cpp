#include <Physics.h>

void Physics::BeginContact(b2Contact* contact)
{
    if (contact->GetFixtureA()->GetBody()->GetType() != b2_staticBody) {
        // ========= BULLET - TANK COLLISION =============
        if (contact->GetFixtureA()->GetFriction() == 0.5f && contact->GetFixtureB()->GetFriction() == 0.3f) {
            void* bodyTankUD = contact->GetFixtureB()->GetBody()->GetUserData();
            void* bodyBulletUD = contact->GetFixtureA()->GetBody()->GetUserData();
            if (bodyBulletUD && bodyTankUD) {
                if (static_cast<Entity*>(bodyTankUD)->getNameId() == "enemy" && static_cast<Bullet*>(bodyBulletUD)->getWhose() != "enemy")
                    static_cast<Entity*>(bodyTankUD)->takeDamage(
                                static_cast<Bullet*>(bodyBulletUD)->getDamage());
                else if (static_cast<Entity*>(bodyTankUD)->getNameId() == "hero")
                    static_cast<Entity*>(bodyTankUD)->takeDamage(
                                static_cast<Bullet*>(bodyBulletUD)->getDamage());

                static_cast<Bullet*>(bodyBulletUD)->setAsDead();
                //std::cout << "Bullet-Tank collision! A\n";
            }
        }
        // ========= BULLET - WALL COLLISION ==============
        else if (contact->GetFixtureA()->GetFriction() == 0.5f) {
            void* bodyBulletUD = contact->GetFixtureA()->GetBody()->GetUserData();
            if (bodyBulletUD) {
                static_cast<Bullet*>(bodyBulletUD)->setAsDead();
                //std::cout << "Bullet-Not Tank collision! A\n";
            }
        }
        // ========= ENEMY - WALL COLLISION ===============
        else if (contact->GetFixtureA()->GetFriction() == 0.3f && contact->GetFixtureB()->GetBody()->GetType() == b2_staticBody) {
            void* bodyTankUD = contact->GetFixtureB()->GetBody()->GetUserData();
            if (bodyTankUD) {
                if (static_cast<Entity*>(bodyTankUD)->getNameId() == "enemy") {
                    static_cast<Enemy*>(bodyTankUD)->setChangeWayTrue();
                    //std::cout << "Enemy-Wall collision! A\n";
                }
            }
        }
        // ========= ENEMY - ENEMY COLLISION ===============
        else if (contact->GetFixtureB()->GetFriction() == 0.3f && contact->GetFixtureA()->GetFriction() == 0.3f) {
            void* bodyTankUD1 = contact->GetFixtureA()->GetBody()->GetUserData();
            void* bodyTankUD2 = contact->GetFixtureB()->GetBody()->GetUserData();
            if (bodyTankUD1 && bodyTankUD2) {
                if (static_cast<Entity*>(bodyTankUD1)->getNameId() == "enemy" &&
                        static_cast<Entity*>(bodyTankUD2)->getNameId() == "enemy") {
                    static_cast<Enemy*>(bodyTankUD1)->setChangeWayTrue();
                    static_cast<Enemy*>(bodyTankUD2)->setChangeWayTrue();
                }
            }
        }
    }

    if (contact->GetFixtureB()->GetBody()->GetType() != b2_staticBody) {
        // ========= BULLET - TANK COLLISION =============
        if (contact->GetFixtureB()->GetFriction() == 0.5f && contact->GetFixtureA()->GetFriction() == 0.3f) {
            void* bodyTankUD = contact->GetFixtureA()->GetBody()->GetUserData();
            void* bodyBulletUD = contact->GetFixtureB()->GetBody()->GetUserData();
            if (bodyBulletUD && bodyTankUD) {
                if (static_cast<Entity*>(bodyTankUD)->getNameId() == "enemy" && static_cast<Bullet*>(bodyBulletUD)->getWhose() != "enemy")
                    static_cast<Entity*>(bodyTankUD)->takeDamage(
                                static_cast<Bullet*>(bodyBulletUD)->getDamage());
                else if (static_cast<Entity*>(bodyTankUD)->getNameId() == "hero")
                    static_cast<Entity*>(bodyTankUD)->takeDamage(
                                static_cast<Bullet*>(bodyBulletUD)->getDamage());
                static_cast<Bullet*>(bodyBulletUD)->setAsDead();
                //std::cout << "Bullet-Tank collision! B\n";
            }
        }
        // ========= BULLET - WALL COLLISION ==============
        else if (contact->GetFixtureB()->GetFriction() == 0.5f) {
            void* bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
            if (bodyUserData) {
                static_cast<Bullet*>(bodyUserData)->setAsDead();
                //std::cout << "Bullet-Not Tank collision! B\n";
            }
        }
        // ========= ENEMY - WALL COLLISION ===============
        else if (contact->GetFixtureB()->GetFriction() == 0.3f && contact->GetFixtureA()->GetBody()->GetType() == b2_staticBody) {
            void* bodyTankUD = contact->GetFixtureB()->GetBody()->GetUserData();
            if (bodyTankUD) {
                if (static_cast<Entity*>(bodyTankUD)->getNameId() == "enemy") {
                        static_cast<Enemy*>(bodyTankUD)->setChangeWayTrue();
                  //  std::cout << "Enemy-Wall collision! A\n";
                }
            }
        }
        // ========= ENEMY - ENEMY COLLISION ===============
        else if (contact->GetFixtureB()->GetFriction() == 0.3f && contact->GetFixtureA()->GetFriction() == 0.3f) {
            void* bodyTankUD1 = contact->GetFixtureA()->GetBody()->GetUserData();
            void* bodyTankUD2 = contact->GetFixtureB()->GetBody()->GetUserData();
            if (bodyTankUD1 && bodyTankUD2) {
                if (static_cast<Entity*>(bodyTankUD1)->getNameId() == "enemy" &&
                        static_cast<Entity*>(bodyTankUD2)->getNameId() == "enemy") {
                    static_cast<Enemy*>(bodyTankUD1)->setChangeWayTrue();
                    static_cast<Enemy*>(bodyTankUD2)->setChangeWayTrue();
                }
            }
        }
    }
}
