#ifndef ENEMY_H
#define ENEMY_H

#include "Entity.h"
#include <list>

class Enemy : public Entity
{
public:
    static void EnemiesFactory(b2World&,      std::list<Enemy*>&);
    static void EnemiesDisplay(RenderWindow&, std::list<Enemy*>&);
    static void EnemiesMoving( b2World&,      std::list<Enemy*>&);
    static bool getLifeStatus( std::list<Enemy*>&);
    static bool HaveDeadEnemy;

    Enemy(float, float, b2World&);
    ~Enemy();

    void    attack(b2World&)        override;
    void    takeDamage(int)         override;
    void    display(RenderWindow&)  override;
    void    moving(b2World&)        override;
    void    setChangeWayTrue();

private:
    bool    changeWay_;
    bool    needSomeMove_;
    Clock   changeWayClock_;
    Time    changeWayTime_;
};

#endif // ENEMY_H
