TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES +=  main.cpp \
            GameLoop.cpp \
            Hero.cpp \
            HUD.cpp \
            Level.cpp \
            MainMenu.cpp \
            Map.cpp \
            Enemy.cpp \
    Bullet.cpp \
    Physics.cpp

HEADERS +=  \
            Entity.h \
            GameLoop.h \
            Hero.h \
            HUD.h \
            Level.h \
            MainMenu.h \
            Map.h \
            Enemy.h \
            constants.h \
            Box2D/Box2D.h \
    Bullet.h \
    Physics.h

LIBS += -L"/home/garvel/Programs/BuildSFML/lib"

CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system
CONFIG(debug, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system

INCLUDEPATH += "/home/garvel/Programs/SFML-master/include"
DEPENDPATH += "/home/garvel/Programs/SFML-master/include"

unix:!macx: LIBS += -L$$PWD/Box2D/ -lBox2D

INCLUDEPATH += $$PWD/Box2D
DEPENDPATH += $$PWD/Box2D

unix:!macx: PRE_TARGETDEPS += $$PWD/Box2D/libBox2D.a
