#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>

using namespace sf;

class MainMenu
{
public:
    MainMenu();
    ~MainMenu();

	bool	display();

private:
    Font		font_;
    Sprite		background_;
    Texture*	backTexture_;

    Text    	l1Start_,
                l2Settings_,
                l3Exit_,
                l1Difficulty_,
                l2Back_;

	void	setLabelStyle(Text&, String, float, float);
};
