#include "Bullet.h"

bool Bullet::HaveDeadBullet = false;
std::list<Bullet*> Bullet::ActiveBullets;

void Bullet::DisplayAll(RenderWindow& window)
{
    if (HaveDeadBullet) {
        for (auto itr = ActiveBullets.begin(); itr != ActiveBullets.end(); ) {
            if ((*itr)->iDead_) {
                Bullet* bullet = *itr;
                delete bullet;
                ActiveBullets.erase(itr++);
            }
             else
                itr++;
        }
        HaveDeadBullet = false;
    }

    for (auto itr = ActiveBullets.begin(); itr != ActiveBullets.end(); itr++)
        (*itr)->display(window);
}

Bullet::Bullet(b2Vec2 vecXY, b2World& world, char direction, int damage, std::string whose, float speed)
{
    damage_ = damage;
    whose_ = whose;

    iDead_ = false;

    bulletTexture_ = new Texture();
    bulletSprite_ = new Sprite();

    if (!bulletTexture_->loadFromFile(PATH + "bullet.png"))
        std::cout << "|  ERROR   | HERO| hero.png texture open error!" << '\n';

    b2BodyDef bodyDef;
    b2Vec2 directionVector;

    switch (direction) {
        case 'N':
            bodyDef.position.Set(vecXY.x * TO_METERS, (vecXY.y-40.0f) * TO_METERS);
            directionVector.x = 0.0f;
            directionVector.y = -speed;
            break;
        case 'S':
            bodyDef.position.Set(vecXY.x * TO_METERS, (vecXY.y+40.0f) * TO_METERS);
            directionVector.x = 0.0f;
            directionVector.y = speed;
            break;
        case 'W':
            bodyDef.position.Set((vecXY.x-40.0f) * TO_METERS, vecXY.y * TO_METERS);
            directionVector.x = -speed;
            directionVector.y = 0.0f;
            break;
        case 'E':
            bodyDef.position.Set((vecXY.x+40.0f) * TO_METERS, vecXY.y * TO_METERS);
            directionVector.x = speed;
            directionVector.y = 0.0f;
    }
    bodyDef.type = b2_dynamicBody;
    bulletPhysicsBody_ = world.CreateBody(&bodyDef);

    b2PolygonShape shape;
    shape.SetAsBox(15.0f/2.0f * TO_METERS, 15.0/2.0f * TO_METERS);

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.5f;

    bulletPhysicsBody_->CreateFixture(&fixtureDef);
    bulletPhysicsBody_->SetUserData(this);
    bulletPhysicsBody_->SetFixedRotation(true);

    bulletSprite_->setTexture(*bulletTexture_);
    bulletSprite_->setPosition(bulletPhysicsBody_->GetPosition().x*TO_PIXELS, bulletPhysicsBody_->GetPosition().y*TO_PIXELS);
    bulletSprite_->setOrigin(7.0f/2.0f, 8.0f/2.0f);

    switch (direction) {
        case 'N':
            bulletPhysicsBody_->SetTransform(bulletPhysicsBody_->GetWorldCenter(), 0.0f);
            break;
        case 'S':
            bulletPhysicsBody_->SetTransform(bulletPhysicsBody_->GetWorldCenter(), 180.0f / DEG);
            break;
        case 'W':
            bulletPhysicsBody_->SetTransform(bulletPhysicsBody_->GetWorldCenter(), 270.0f / DEG);
            break;
        case 'E':
            bulletPhysicsBody_->SetTransform(bulletPhysicsBody_->GetWorldCenter(), 90.0f / DEG);
    }
    bulletSprite_->setRotation(bulletPhysicsBody_->GetAngle() * DEG);


    bulletPhysicsBody_->SetLinearVelocity(directionVector);
}

Bullet::~Bullet()
{
    delete bulletTexture_;
    delete bulletSprite_;
    bulletPhysicsBody_->GetWorld()->DestroyBody(bulletPhysicsBody_);
}

void Bullet::display(RenderWindow& window)
{
    bulletSprite_->setPosition(bulletPhysicsBody_->GetPosition().x*TO_PIXELS, bulletPhysicsBody_->GetPosition().y*TO_PIXELS);

    window.draw(*bulletSprite_);
}

void Bullet::setAsDead()
{
    iDead_ = true;
    Bullet::HaveDeadBullet = true;
}

int Bullet::getDamage()
{
    return damage_;
}

b2Body& Bullet::getBody()
{
    return *bulletPhysicsBody_;
}

std::string Bullet::getWhose()
{
    return whose_;
}
